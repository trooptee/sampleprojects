package main.java.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;
import java.util.logging.Logger;
import main.java.HuntSteps;

/**
 * This is an Util class which provides the implementation of necessary methods
 * for hunting Treasure
 * 
 * @author Trupti
 */

public class HuntUtil {
	private static final Logger LOGGER = Logger.getLogger(HuntUtil.class.getName());

	/**
	 * This method calculates the distance based on the speed and time. Based on
	 * the distance calculation formula a distance is returned for N,S,E or W
	 * directions. However, if the direction is NW, NE, SW or SE then the
	 * vertical and horizontal distance is calculated based on an 'isosceles
	 * right triangle' equation.
	 * 
	 * @param huntStep
	 * @param isHypotenuse
	 * @param propertiesFilePath
	 * @return distance
	 */
	public static double getDistance(HuntSteps huntStep, boolean isHypotenuse, String propertiesFilePath) {
		double distance = 0;
		double speed = getSpeed(huntStep, propertiesFilePath);
		double time = huntStep.getTime();

		distance = speed * time;

		if (isHypotenuse) {
			/* For an 'isosceles right triangle' a^2 + b^2 = c^2 where a = b */
			distance = Math.sqrt((distance * distance) / 2);
		}
		return distance;
	}

	/**
	 * This method reads a properties file that contains the speed for every
	 * Mode of Transportation
	 * 
	 * @param huntStep
	 * @param propertiesFilePath
	 * @return speed
	 */
	private static double getSpeed(HuntSteps huntStep, String propertiesFilePath) {
		Properties prop = new Properties();
		InputStream input = null;
		double speed = 0;
		try {

			input = new FileInputStream(propertiesFilePath);

			/* load properties file */
			prop.load(input);

			/* get the property value */
			String speedStr = prop.getProperty(huntStep.getTransportationMode().name());
			speed = Double.parseDouble(speedStr.substring(0, speedStr.indexOf('m')));
		} catch (IOException ex) {
			LOGGER.info("IOException while loading properties file : " + ex);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					LOGGER.info("IOException while closing properties file : " + e);
				}
			}
		}
		return speed;
	}

	/**
	 * This method return the duration which is equal to the number of hours
	 * 
	 * @param inputDuration
	 * @return duration in hours
	 */
	public static double getDuration(String inputDuration) {
		// replace hour/min/secs strings for H, M and S
		String durationStr = inputDuration.replaceAll("\\s*(hour|hr)s?\\s*", "H");
		durationStr = durationStr.replaceAll("\\s*mins?\\s*", "M");
		durationStr = durationStr.replaceAll("\\s*secs?\\s*", "S");
		durationStr = durationStr.trim();
		Duration d = Duration.parse("PT" + durationStr);

		double hours = d.toMillis() / 3600000d;
		return hours;
	}
}

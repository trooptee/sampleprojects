package main.java;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import main.java.enums.Direction;
import main.java.enums.TransportationMode;
import main.java.utils.HuntUtil;

/**
 * This is the main class. Run this class to get the final destination/treasure.
 * 
 * @author Trupti
 */

public class Hunt {

	private static final Logger LOGGER = Logger.getLogger(Hunt.class.getName());

	/*
	 * This is the input text file which contains the information - Direction,
	 * Mode of Transportation and Duration
	 */
	private final String filePathName = "./src/main/resources/huntSteps.txt";

	/*
	 * This is a properties file which contain the information - Mode of
	 * Transportation and Speed
	 */
	private final String propertiesFilePath = "./src/main/resources/speed.properties";

	public static void main(String[] args) {
		Hunt treasureHunt = new Hunt();

		/*
		 * Create a list of HuntSteps. Every HuntSteps Object maps to the input
		 * file information.
		 */
		List<HuntSteps> huntStepsList = treasureHunt.getHuntSteps();

		/*
		 * Once we have all the steps needed to be traced, execute those steps
		 * to find the Treasure.
		 */
		treasureHunt.findTreasure(huntStepsList);
	}

	/**
	 * This method takes List of Treasure Hunt Steps as input and prints the
	 * treasure location as output
	 * 
	 * @param huntStepsList
	 */
	private void findTreasure(List<HuntSteps> huntStepsList) {
		double northSouthDistance = 0;
		double eastWestDistance = 0;
		double distance = 0;

		for (HuntSteps huntStep : huntStepsList) {
			switch (huntStep.getDirection()) {
			case NORTH:
				distance = HuntUtil.getDistance(huntStep, false, propertiesFilePath);
				northSouthDistance = northSouthDistance + distance;
				break;
			case SOUTH:
				distance = HuntUtil.getDistance(huntStep, false, propertiesFilePath);
				northSouthDistance = northSouthDistance - distance;
				break;
			case EAST:
				distance = HuntUtil.getDistance(huntStep, false, propertiesFilePath);
				eastWestDistance = eastWestDistance + distance;
				break;
			case WEST:
				distance = HuntUtil.getDistance(huntStep, false, propertiesFilePath);
				eastWestDistance = eastWestDistance - distance;
				break;
			case NORTHEAST:
				distance = HuntUtil.getDistance(huntStep, true, propertiesFilePath);
				northSouthDistance = northSouthDistance + distance;
				eastWestDistance = eastWestDistance + distance;
				break;
			case NORTHWEST:
				distance = HuntUtil.getDistance(huntStep, true, propertiesFilePath);
				northSouthDistance = northSouthDistance + distance;
				eastWestDistance = eastWestDistance - distance;
				break;
			case SOUTHEAST:
				distance = HuntUtil.getDistance(huntStep, true, propertiesFilePath);
				northSouthDistance = northSouthDistance - distance;
				eastWestDistance = eastWestDistance + distance;
				break;
			case SOUTHWEST:
				distance = HuntUtil.getDistance(huntStep, true, propertiesFilePath);
				northSouthDistance = northSouthDistance - distance;
				eastWestDistance = eastWestDistance - distance;
				break;
			}
		}

		/* Round the numbers in order to display approximate distance */
		long northSouthRoundedDistance = Math.round(northSouthDistance);
		long eastWestRoundedDistance = Math.round(eastWestDistance);

		LOGGER.info("Rounding the miles to the nearest number --> \nTreasure is located at : ");
		if (northSouthRoundedDistance > 0) {
			LOGGER.info(northSouthRoundedDistance + " miles to the North");
		} else {
			LOGGER.info(northSouthRoundedDistance * -1 + " miles to the South");
		}
		if (eastWestRoundedDistance > 0) {
			LOGGER.info(eastWestRoundedDistance + " miles to the East");
		} else {
			LOGGER.info(eastWestRoundedDistance * -1 + " miles to the West");
		}

		/* Print accurate numbers */
		LOGGER.info("Accurate information --> \nTreasure is located at : \n" + northSouthDistance
				+ " miles to the North (if positive) and South (if negative) \n" + eastWestDistance
				+ " miles to the East (if positive) and West (if negative).");
	}

	/**
	 * This method will : 1. read the input file 2. map every step to the
	 * HuntSteps Object 3. set all the information from the input file into the
	 * HuntSteps Object
	 * 
	 * @return List of Steps
	 */
	private List<HuntSteps> getHuntSteps() {
		FileReader input = null;
		List<HuntSteps> huntStepsList = null;
		try {
			/* read treasure hunt steps from a file */
			File file = new File(filePathName);
			input = new FileReader(file);
			BufferedReader bufRead = new BufferedReader(input);
			String stepStr = null;
			huntStepsList = new ArrayList<HuntSteps>();
			while ((stepStr = bufRead.readLine()) != null) {
				String[] stepArray = stepStr.split(",");
				/* Create a HuntSteps object */
				HuntSteps aStep = new HuntSteps();
				/* Set Transportation Mode */
				aStep.setTransportationMode(TransportationMode.valueOf(stepArray[0].trim()));
				/*
				 * convert the time into hours for further calculation and set
				 * it
				 */
				double hours = HuntUtil.getDuration(stepArray[1]);
				aStep.setTime(hours);

				/* Set Direction */
				aStep.setDirection(Direction.valueOf(stepArray[2].trim()));

				/* Add every HuntSteps object to a List of TreasureHuntSteps */
				huntStepsList.add(aStep);
			}
		} catch (FileNotFoundException fileNotFoundException) {
			LOGGER.info("File " + filePathName + " not found : " + fileNotFoundException);
		} catch (IOException ioException) {
			LOGGER.info("System encountered an IOException : " + ioException);
		}
		return huntStepsList;
	}

}

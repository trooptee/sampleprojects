package main.java.enums;

public enum TransportationMode {
	WALK, RUN, HORSE_TROT, HORSE_GALLOP, ELEPHANT_RIDE
}

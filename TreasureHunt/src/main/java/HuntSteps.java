package main.java;

import main.java.enums.TransportationMode;
import main.java.enums.Direction;

/**
 * This class maps to the input file information
 * 
 * @author Trupti
 */

public class HuntSteps {
	private Direction direction;
	private TransportationMode transportationMode;
	private double time;

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public TransportationMode getTransportationMode() {
		return transportationMode;
	}

	public void setTransportationMode(TransportationMode transportationMode) {
		this.transportationMode = transportationMode;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

}

package services.recordServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import services.recordServices.repo.ContactRecordDAO;

/**
 * This class is a REST Controller which processes the update, delete, create and get requests for a Contact Record.  
 * @author Trupti
 *
 */

@RestController
@RequestMapping(value="/contactRecord/")
public class RecordController {
	
	private static String SUCCESS = "SUCCESS";
	
	@Autowired
	private ContactRecordDAO contactRecordDAO;

	/**
	 * This service retrieves a Contact Record object based on the key (name of the Contact)
	 * @param name
	 * @return ResponseEntity
	 */
	@RequestMapping(value="/retrieve/{name}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ContactRecord>  getContactRecordByName(@PathVariable String name){
		ContactRecord result = contactRecordDAO.getRecordByName(name);
		if(result != null){
			return new ResponseEntity<ContactRecord>(result, HttpStatus.OK);
		}else{
			return new ResponseEntity<ContactRecord>(HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * This service saves a Contact Record object. 
	 * @param contactRecord
	 * @return ResponseEntity
	 */
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public ResponseEntity<String> createContactRecord(@RequestBody ContactRecord contactRecord){
		String result = contactRecordDAO.saveRecord(contactRecord);
		if(result == SUCCESS){
			return new ResponseEntity<String>("Contact Record with name - " + contactRecord.getName() + " has been successfully created.", HttpStatus.OK);	
		}else{
			return new ResponseEntity<String>("Contact Record with name - " + contactRecord.getName() + " already exist.", HttpStatus.CONFLICT);
		}
	}

	/**
	 * This service updates a Contact Record object. 
	 * @param contactRecord
	 * 
	 * @return ResponseEntity
	 */
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public ResponseEntity<String> updateContactRecord(@RequestBody ContactRecord contactRecord){
		String result = contactRecordDAO.updateRecord(contactRecord);
		if(result == SUCCESS){
			return new ResponseEntity<String>("Contact Record with name - " + contactRecord.getName() + " has been successfully updated.", HttpStatus.OK);	
		}else{
			return new ResponseEntity<String>("Contact Record with name - " + contactRecord.getName() + " does not exist.", HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * This method deletes a Contact Record object based on the key(name)
	 * @param name
	 * @return ResponseEntity
	 */
	@RequestMapping(value="/delete/{name}", method=RequestMethod.DELETE)
	public ResponseEntity<String> deleteContactRecordByName(@PathVariable String name){
		String result = contactRecordDAO.deleteRecord(name);
		if(result == SUCCESS){
			return new ResponseEntity<String>("Contact Record with name - " + name + " has been successfully deleted.", HttpStatus.OK);	
		}else{
			return new ResponseEntity<String>("Contact Record with name - " + name + " does not exist.", HttpStatus.NOT_FOUND);
		}
	}
}

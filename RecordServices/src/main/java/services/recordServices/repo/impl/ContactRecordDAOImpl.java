package services.recordServices.repo.impl;

import java.util.HashMap;
import org.springframework.stereotype.Repository;
import services.recordServices.ContactRecord;
import services.recordServices.repo.ContactRecordDAO;

/**
 * This class performs CRUD operations on the contact record data.
 * @author Trupti
 *
 */
@Repository
public class ContactRecordDAOImpl implements ContactRecordDAO{
	
	/** A data structure to temporarily persist data. 
	 *  Key is contact's name
	 **/
	HashMap<String, ContactRecord> contactRecordStorageMap = new HashMap<String, ContactRecord>();
	
	private static String SUCCESS = "SUCCESS";
	private static String FAILURE = "FAILURE";

	public String saveRecord(ContactRecord contactRecord){
		if(contactRecordStorageMap.get(contactRecord.getName()) == null){
			contactRecordStorageMap.put(contactRecord.getName(), contactRecord);
			return SUCCESS;
		}else{
			return FAILURE;
		}
	}
	
	public ContactRecord getRecordByName(String name){
		if(contactRecordStorageMap.get(name) == null){
			return null;
		}else{
			return contactRecordStorageMap.get(name);
		}
	}
	
	public String updateRecord(ContactRecord contactRecord){
		if(contactRecordStorageMap.get(contactRecord.getName()) == null){
			return FAILURE;
		}else{
			contactRecordStorageMap.replace(contactRecord.getName(), contactRecord);
			return SUCCESS;
		}
	}
	
	public String deleteRecord(String name){
		if(contactRecordStorageMap.get(name) == null){
			return FAILURE;
		}else{
			contactRecordStorageMap.remove(name);
			return SUCCESS;
		}
	}

}

package services.recordServices.repo;

import services.recordServices.ContactRecord;

/**
 * This interface declares CRUD methods for contact record data
 * @author Trupti
 *
 */
public interface ContactRecordDAO {

	public String saveRecord(ContactRecord contactRecord);
	
	public ContactRecord getRecordByName(String name);
	
	public String updateRecord(ContactRecord contactRecord);
	
	public String deleteRecord(String name);

}
